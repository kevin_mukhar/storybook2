package edu.cspsaa.sb;

import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;

import android.annotation.TargetApi;
import android.app.DialogFragment;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements
                OnGestureListener {

    private static final String TAG = MainActivity.class.getCanonicalName();

    private int idx = 0;

    private int[] imageRes = new int[] { R.drawable.title, R.drawable.tom08,
                    R.drawable.tom11, R.drawable.tom12, R.drawable.tom15,
                    R.drawable.tom16, R.drawable.tom19, R.drawable.tom20,
                    R.drawable.tom23, R.drawable.tom24, R.drawable.tom27,
                    R.drawable.tom28, R.drawable.tom31, R.drawable.tom32,
                    R.drawable.tom35, R.drawable.tom36, R.drawable.tom39,
                    R.drawable.tom40, R.drawable.tom43, R.drawable.tom44,
                    R.drawable.tom47, R.drawable.tom48, R.drawable.tom51,
                    R.drawable.tom52, R.drawable.tom55, R.drawable.tom57,
                    R.drawable.tom58, R.drawable.credits };

    private String[] textFiles = new String[] { "title.txt", "tom08.txt",
                    "tom11.txt", "tom12.txt", "tom15.txt", "tom16.txt",
                    "tom19.txt", "tom20.txt", "tom23.txt", "tom24.txt",
                    "tom27.txt", "tom28.txt", "tom31.txt", "tom32.txt",
                    "tom35.txt", "tom36.txt", "tom39.txt", "tom40.txt",
                    "tom43.txt", "tom44.txt", "tom47.txt", "tom48.txt",
                    "tom51.txt", "tom52.txt", "tom55.txt", "tom57.txt",
                    "tom58.txt", "credits.txt" };

    private int[] audioRes = new int[] { R.raw.title, R.raw.tom08, R.raw.tom11,
                    R.raw.tom12, R.raw.tom15, R.raw.tom16, R.raw.tom19,
                    R.raw.tom20, R.raw.tom23, R.raw.tom24, R.raw.tom27,
                    R.raw.tom28, R.raw.tom31, R.raw.tom32, R.raw.tom35,
                    R.raw.tom36, R.raw.tom39, R.raw.tom40, R.raw.tom43,
                    R.raw.tom44, R.raw.tom47, R.raw.tom48, R.raw.tom51,
                    R.raw.tom52, R.raw.tom55, R.raw.tom57, R.raw.tom58,
                    R.raw.credits };

    private MediaPlayer mPlayer;

    private ImageView imageView;

    private GestureDetectorCompat mDetector;

    private TextView textView;

    private Hashtable<Integer, String> textMap = new Hashtable<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.imageView1);
        Log.d(TAG, "Height: " + imageView.getHeight());
        Log.d(TAG, "Width: " + imageView.getWidth());
        textView = (TextView) findViewById(R.id.textView2);
        mDetector = new GestureDetectorCompat(this, this);
        mPlayer = MediaPlayer.create(this, R.raw.title);
        startAudio(3000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.mDetector.onTouchEvent(event);
        // Be sure to call the superclass implementation
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        if (mPlayer.isPlaying())
            mPlayer.pause();
        else
            mPlayer.start();
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                    float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                    float velocityY) {
        if (velocityX > 0)
            idx--;
        else
            idx++;

        if (idx < 0)
            idx = 0;
        if (idx == imageRes.length)
            idx--;

        setNewImage();
        try {
            setNewText();
        } catch (IOException e) {
            e.printStackTrace();
        }
        loadNewAudio();

        return true;
    }

    private void loadNewAudio() {
        mPlayer.stop();
        mPlayer.release();
        mPlayer = MediaPlayer.create(this, audioRes[idx]);
        startAudio(500);
    }

    private void startAudio(final int delay) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(delay);
                    mPlayer.start();
                } catch (InterruptedException ignored) {
                }
            }
        });
        t.start();
    }

    private void setNewText() throws IOException {
        String text = textMap.get(idx);
        if (text == null) {
            InputStream is = getAssets().open(textFiles[idx]);
            int size = is.available();

            // Read the entire asset into a local byte buffer.
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            // Convert the buffer into a string.
            text = new String(buffer);
            textMap.put(idx, text);
        }

        // Finally stick the string into the text view.
        textView.setText(text);
    }

    private void setNewImage() {
        imageView.setImageResource(imageRes[idx]);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void item1Clicked(MenuItem item) {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            DialogFragment f = new AboutDialogFragment();
            f.show(getFragmentManager(), "about");
        } else {
            Intent i = new Intent(getApplicationContext(), AboutActivity.class);
            startActivity(i);
        }
    }
}
