package edu.cspsaa.sb;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class AboutActivity extends Activity {

    private static final String TAG = AboutActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        TextView textView = (TextView) findViewById(R.id.abouttext);
        textView.setText(R.string.about_default);

        Log.d(TAG, "text view text is " + textView.getText());
        InputStream is = null;
        try {
            is = getAssets().open("credits.txt");
            int size = is.available();

            // Read the entire asset into a local byte buffer.
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            // Convert the buffer into a string.
            String text = new String(buffer);
            Log.d(TAG, "Retrieved: " + text);

            // Finally stick the string into the text view.
            textView.setText(text);
        } catch (IOException e) {
        } finally {
            if (is != null)
                try {
                    is.close();
                } catch (IOException e) {
                }
        }

        View okButton = findViewById(R.id.aboutOkBtn);
        okButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),
                                MainActivity.class);
                startActivity(i);
            }
        });
    }
}
