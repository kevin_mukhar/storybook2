package edu.cspsaa.sb;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * A simple {@link Fragment} subclass. Activities that contain this fragment must implement the
 * {@link AboutDialogFragment.OnFragmentInteractionListener} interface to handle interaction events.
 * 
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class AboutDialogFragment extends DialogFragment {
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.about_message).setPositiveButton(
                        R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
